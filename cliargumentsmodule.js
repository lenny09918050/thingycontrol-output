// Generated by CoffeeScript 2.5.1
(function() {
  var cliargumentsmodule, extractMeowed, getHelpText, getOptions, log, meow;

  cliargumentsmodule = {
    name: "cliargumentsmodule"
  };

  //region node_modules
  meow = require("meow");

  //endregion

  //log Switch
  log = function(arg) {
    if (allModules.debugmodule.modulesToDebug["cliargumentsmodule"] != null) {
      console.log("[cliargumentsmodule]: " + arg);
    }
  };

  //#initialization function  -> is automatically being called!  ONLY RELY ON DOM AND VARIABLES!! NO PLUGINS NO OHTER INITIALIZATIONS!!
  cliargumentsmodule.initialize = function() {
    return log("cliargumentsmodule.initialize");
  };

  //region internal functions
  getHelpText = function() {
    log("getHelpText");
    return `Usage
    $ thingycontrol
    
Options
    optional:
        --configure, -c
            flag, if available then we start configuring our userConfig first

Examples
    $ thingycontrol -c
    ...`;
  };

  getOptions = function() {
    log("getOptions");
    return {
      flags: {
        configure: {
          type: "boolean",
          alias: "c"
        }
      }
    };
  };

  extractMeowed = function(meowed) {
    var configure;
    log("extractMeowed");
    configure = false;
    if (meowed.flags.configure) {
      configure = true;
    }
    return {configure};
  };

  //endregion

  //region exposed functions
  cliargumentsmodule.extractArguments = function() {
    var extract, meowed, options;
    log("cliargumentsmodule.extractArguments");
    options = getOptions();
    meowed = meow(getHelpText(), getOptions());
    extract = extractMeowed(meowed);
    return extract;
  };

  //endregion exposed functions
  module.exports = cliargumentsmodule;

}).call(this);
